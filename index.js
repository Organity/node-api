import Koa from 'koa';
import Sequelize from 'sequelize';

const NODE_ENV = 'development';
const PORT = 3000;

// Sequelize setup
const sequelize = new Sequelize('test', 'test', 'test', {
  host: 'db',
  dialect: 'postgres',
  operatorAliases: false,
  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  },
  define: {
    timestamps: false
  }
})

const User = sequelize.define('user', {
  firstName: { type: Sequelize.STRING },
  lastName: { type: Sequelize.STRING },
  email: { type: Sequelize.STRING },
  password: { type: Sequelize.STRING }
})

User.sync({ force: true }).then(() => {
  return User.create({
    firstName: 'John',
    lastName: 'Wick',
    email: 'babayaga@gmail.com',
    password: 'ILovePuppies'
  })
})

User.findAll().then(users => {
  console.log(users)
})

// Koa setup
const app = new Koa();

app.use(async ctx => {
  ctx.body = {
    status: 'success',
    message: 'hello, world'
  };
});

app.listen(PORT, () => {
  console.log(`listening in PORT: ${ PORT }`)
});
